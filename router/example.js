const express = require('express');
const router = new express.Router();
const exampleController = require('../controller/example/index');

router.get('/', exampleController.get);

module.exports = router;
