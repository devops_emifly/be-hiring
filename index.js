const port = 9011
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const exampleRouter = require('./router/example')
const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('./docs/swagger')


const app = express();
const router = express.Router();

app.use(express.static(__dirname + '/public'));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

router.use('/example', exampleRouter)

app.listen(port, () => console.log('server running on port '+port+'!'));
