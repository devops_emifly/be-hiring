# Backend Test Emifly
### Deskripsi Test
Budi memiliki 3 buah gelas koin input yang didalamnya terdiri dari beberapa jenis koin tertentu. Budi perlu mengklasifikasikan koin-koin tersebut berdasarkan kualitas dari koin yang di-input menjadi beberapa klasifikasi output. Pencatatan koin akan dihitung setiap Budi memasukan koin ke dalam gelas output, dan koin yang dimasukkan ke dalam gelas output perlu dicatat juga gelas inputnya (relasi ini akan disimpan pada pivot table dan pencatatan koin yang dimasukan akan disimpan pada tally table). 

Budi kemudian diminta membuat program untuk melakukan pencatatan proses pengelompokan koin tersebut. Budi juga sudah membuat model database program tersebut pada file Tugas Hiring Backend.mwb dan dapat dibuka menggunakan aplikasi MySQL Workbench. Selain program pencatatan, Budi juga ingin mendapatkan laporan detail dari proses pengklasifikasian koin tersebut. Bantulah Budi untuk membuat Node JS Express API yang men-generate file excel yang dibuat dengan format seperti pada file Target Excel (terdapat pada folder /docs/template). Adapun spesifikasi program pengklasifikasian koin harus mengikuti ketentuan berikut:

### Requirements 
- Menggunakan Express dan Router sebagai konstruktor dari REST API
- Menggunakan library Excel Generator "exceljs" untuk mengenerate laporan excel yang diinginkan oleh Budi 
- Menggunakan ORM Sequelize untuk mengakses database (model dibuat sendiri berdasarkan model db yang ada pada file Tugas Hiring Backend.mwb)
- Membuat dokumentasi API menggunakan library "swagger-ui-express" dan "swagger-jsdoc"

Pengumpulan dapat dilakukan dengan mengirimkan email seluruh program yang di-zip ke devops@emifly.com sebelum Selasa, 4 Mei 2021 Jam 12:00 siang. Jika ada bagian yang dirasa kurang jelas, dapat menambahkan asumsi dan tuliskan asumsi yang digunakan ke dalam file asumsi.txt yang dimasukkan ke dalam file zip test.
