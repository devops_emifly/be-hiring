const { response } = require('express');
const db = require('../../models/index');
const get = async (req, res) => {
    try {
        const responsedata = {
            '1':'1',
            '2':'2',
            '3':'3'
        }
        res.status(200).send({ message: 'Get example success', responsedata });  
    } catch (e) {
        res.status(400).send({ message: e.message });
    }
}

module.exports = {
    get
};  